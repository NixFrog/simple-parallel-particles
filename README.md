# Modélisation simple de particules

Ce projet vise à implémenter un système de particules en 2D sans collisions entre elles, dans trois frameworks de parallélisation: OpenMP, OpenCL et CUDA.

## Algorithme réalisé

L'algorithme est simple: à chaque tour de boucle, créer N particules dont la vitesse de départ est un entier relatif aléatoire sur X et un entier naturel sur Y, puis mettre à jour toutes les particules selon leur vitesse et accélération, en suivant la gravité. Si une particule dépasse un certain âge, elle retourne à sa position initiale.

Il est possible de supprimer les particules, mais le redimensionnement d'un tableau est une opération très coûteuse, et le programme ne tourne que pour un nombre d'itération définit, et ne risque donc pas de se bloquer. J'ai donc choisi de les bloquer à leur position initiale; s'il était nécessaire d'effectuer un rendu en temps réel on pourrait réinitialiser les particules quand elles atteignent leur temps de vie maximal, mais je préfère conserver l'idée qu'elles sont "mortes".

## Dépendences

* OpenMp: un compilateur C++ supportant openMP et C++1z
* OpenCL: bibliothèque de code et SDK, ainsi qu'un driver compatible
* CUDA: le toolkit fourni par NVIDIA et une carte avec un driver compatible

## Build

* OpenMP: j'utilise le système de build Meson, qui permet de générer des fichiers ninja. Pour compiler le projet, faire depuis la racine OpenMP:

```
mkdir -p build && cd build && meson .. && ninja
```

* CUDA: meson ne supporte malheureusement pas nvcc. Il faut donc le lancer en ligne de commande:

```
nvcc main.cu -o cuda-particles.out
```

* OpenCL: il suffit de lancer la commande suivante

```
clang++ -std=c99 main.c -lOpenCL -o opencl-particles.out
```

## Exécution

Le programme prend deux paramètres en entrée: le nombre d'itérations à effectuer, et le nombre de particules à générer à chaque itération.

Les résultats sont écrit sur la sortie standard; il est ainsi facile de piper directement vers un programme de rendu du choix de l'utilisateur. Si jamais un fichier est requis, le résultat peut être pipé dans le fichier. Par exemple, depuis openmp/build, lancer:

```
./src/openmp-particles/openmp-particles.out 100 50000 > output
```

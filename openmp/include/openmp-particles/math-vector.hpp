#pragma once

#include <vector>
#include <iostream>

namespace OpenMPParticleSystem
{
    class MathVector
    {
    public:
        MathVector(int x, int y): mX(x), mY(y) {}

        void setX(int x) {mX = x;}
        void setY(int y) {mY = y;}
        int getX() const {return mX;}
        int getY() const {return mY;}

        friend MathVector operator+(const MathVector& lhs, const MathVector& rhs);
        friend std::ostream& operator<<(std::ostream& os, const MathVector& mathVector);

    private:
        int mX, mY;
    };

    inline MathVector operator+(const MathVector& lhs, const MathVector& rhs)
    {
        return MathVector(lhs.mX + rhs.mX, lhs.mY + rhs.mY);
    }

    inline std::ostream& operator<<(std::ostream& os, const MathVector& mathVector)
    {
        os << mathVector.mX << " " << mathVector.mY;
        return os;
    }

}

#pragma once

#include <vector>
#include <iostream>

#include <openmp-particles/particle.hpp>

namespace OpenMPParticleSystem
{
    class ParticleSystem
    {
    public:
        ParticleSystem(unsigned int particleCount, int radius, int particleLifetime, int maxYVelocity);
        void generateParticles();
        void update(int elapsedTime);

        friend std::ostream& operator<<(std::ostream& os, const ParticleSystem& particleSystem);

    private:
        std::vector<Particle> mParticles;
        int mGeneratedParticles, mRadius, mParticleLifetime, mMaxYVelocity;
    };

    inline std::ostream& operator<<(std::ostream& os, const ParticleSystem& particleSystem)
    {
        for(const auto& particle : particleSystem.mParticles){
            os << particle.getCoordinates() << std::endl;
        }
        return os;
    }
}

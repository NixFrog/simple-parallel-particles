#pragma once

#include <openmp-particles/math-vector.hpp>

namespace OpenMPParticleSystem
{
    class Particle
    {
    public:
        Particle(MathVector coordinates, MathVector velocity, MathVector acceleration, int age)
            : mCoordinates(coordinates)
            , mVelocity(velocity)
            , mAcceleration(acceleration)
            , mAge(age)
        {}

        void setCoordinates(const MathVector& coordinates) {mCoordinates = coordinates;}
        void setVelocity(const MathVector& velocity) {mVelocity = velocity;}
        void setVelocity(float vx, float vy) {mVelocity.setX(vx); mVelocity.setY(vy);}
        void setAge(int age) {mAge = age;}

        MathVector getCoordinates() const {return mCoordinates;}
        MathVector getVelocity() const {return mVelocity;}
        MathVector getAcceleration() const {return mAcceleration;}
        int getAge() const {return mAge;}

    private:
        MathVector mCoordinates, mVelocity, mAcceleration;
        int mAge;
    };
}

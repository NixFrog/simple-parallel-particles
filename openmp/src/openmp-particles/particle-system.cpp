#include <openmp-particles/particle-system.hpp>
#include <openmp-particles/math-vector.hpp>
#include <random>

using namespace OpenMPParticleSystem;

static const float HEIGHT = 5.0f;
static const float GRAVITY = 9.81f;
static const int FALL_SPEED = GRAVITY * 2.0 * HEIGHT;
static const MathVector START_COORDS(0, 0);

ParticleSystem::ParticleSystem(unsigned int particleCount, int radius, int particleLifetime, int maxYVelocity)
    : mGeneratedParticles(particleCount)
    , mRadius(radius)
    , mParticleLifetime(particleLifetime)
    , mMaxYVelocity(maxYVelocity)
{
    generateParticles();
}

void ParticleSystem::generateParticles()
{
    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<int> velocityX(-mRadius, mRadius);
    std::uniform_int_distribution<int> velocityY(0, mMaxYVelocity);

    MathVector startingVelocity(velocityX(generator), velocityY(generator));
    MathVector acceleration(0, -1);

    Particle particle(START_COORDS, startingVelocity, acceleration, 0);

    for(int i = 0; i < mGeneratedParticles; ++i){
        particle.setVelocity(velocityX(generator), velocityY(generator));
        mParticles.push_back(particle);
    }
}

void ParticleSystem::update(int elapsedTime)
{
    #pragma omp parallel for
    for(auto it = mParticles.begin(); it < mParticles.end(); it++){
        auto& particle = *it;
        particle.setAge(particle.getAge() + 1);
        if(particle.getAge() > mParticleLifetime){
            mParticles.erase(it);
        }
        else{
            particle.setVelocity(particle.getVelocity() + particle.getAcceleration());
            particle.setCoordinates(particle.getCoordinates() + particle.getVelocity());
        }
    }
}

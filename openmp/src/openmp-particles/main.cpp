#include <iostream>

#include <openmp-particles/particle-system.hpp>

int main (int argc, char *argv[])
{
    if(argc != 3){
        std::cerr << "2 arguments required: $openmp-particles <iterations> <particles generated>" << std::endl;
        exit(EXIT_FAILURE);
    }

    int iterations = atoi(argv[1]);
    int generationCount = atoi(argv[2]);

    OpenMPParticleSystem::ParticleSystem particleSystem(generationCount, 20, 100, 50);

    for(int i = 0; i < iterations; i++){
        particleSystem.update(1);
        particleSystem.generateParticles();
    }

    std::cout << particleSystem;
}

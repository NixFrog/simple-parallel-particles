#include <stdio.h>
#include <stdlib.h>

#define TOTAL_THREADS 64

struct V2 {
	int x;
	int y;
};

struct Particle {
	V2 coordinates;
	V2 speed;
	V2 acceleration;
	int age;
};

__global__
void updateParticles(Particle* particles, int particleCount, int time, int lifespan) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if(i < particleCount){
		particles[i].age += time;

		if(particles[i].age > lifespan){
			particles[i].coordinates.x = 0;
			particles[i].coordinates.y = 0;
			particles[i].speed.x = 0;
			particles[i].speed.y = 0;
		}
		else{

			particles[i].coordinates.x += particles[i].speed.x * time
							+ particles[i].acceleration.x * time * time;
			particles[i].coordinates.y += particles[i].speed.y * time
							+ particles[i].acceleration.y * time * time;

			particles[i].speed.x += particles[i].acceleration.x;
			particles[i].speed.y += particles[i].acceleration.y;
		}
	}
}

void init(Particle* particles, int beginIndex, int endIndex, int rangeX, int rangeY);

int main(int argc, char* argv[]) {
	if(argc != 3){
		printf("Wrong number of arguments: <iterations> <particles generated>\n");
		exit(EXIT_FAILURE);
	}

	int iterations = atoi(argv[1]);
	int generatedParticles = atoi(argv[2]);
	int totalParticles = iterations * generatedParticles;
	int particlesSize = sizeof(Particle) * totalParticles;
	int blocks = totalParticles / TOTAL_THREADS + 1;

	Particle *hParticles = (Particle *) malloc(particlesSize);

	Particle *dParticles;
	cudaMalloc((void **)&dParticles, particlesSize);

    init(hParticles, 0, totalParticles, 10, 10);

	for(int i = 0; i < iterations; i++){
		init(hParticles, generatedParticles * i, generatedParticles * (i + 1), 10, 10);
	    cudaMemcpy(dParticles, hParticles, particlesSize, cudaMemcpyHostToDevice);
    	updateParticles<<<blocks, TOTAL_THREADS>>>(dParticles, totalParticles, 1, 50);
		cudaMemcpy(hParticles, dParticles, particlesSize, cudaMemcpyDeviceToHost);
	}

    for (int i = 0; i<totalParticles; ++i) {
        printf("%d %d\n", hParticles[i].coordinates.x, hParticles[i].coordinates.y);
    }

    cudaFree(dParticles);

    exit(EXIT_SUCCESS);
}

void init(Particle* particles, int beginIndex, int endIndex, int rangeX, int rangeY){
	for(int i = beginIndex; i < endIndex; i++){
		particles[i].coordinates.x = 0;
		particles[i].coordinates.y = 0;
		particles[i].speed.x = rand() %(rangeX * 2 + 1) - rangeX;
		particles[i].speed.y = rand() %rangeY;
		particles[i].acceleration.x = 0;
		particles[i].acceleration.y = - 5;
		particles[i].age = 0;
	}
}

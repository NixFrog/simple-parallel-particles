typedef struct tag_V2{
	int x;
	int y;
}V2;

typedef struct tag_Particle{
	V2 coordinates;
	V2 speed;
	V2 acceleration;
	int age;
}Particle;

__kernel void update_particles(__global Particle* particles, const int particleCount, const int time, const int lifespan)
{
	int i = get_global_id(0);

	if(i < particleCount){
		particles[i].age += time;

		if(particles[i].age > lifespan){
			particles[i].coordinates.x = 0;
			particles[i].coordinates.y = 0;
			particles[i].speed.x = 0;
			particles[i].speed.y = 0;
		}
		else{

			particles[i].coordinates.x += particles[i].speed.x * time
							+ particles[i].acceleration.x * time * time;
			particles[i].coordinates.y += particles[i].speed.y * time
							+ particles[i].acceleration.y * time * time;

			particles[i].speed.x += particles[i].acceleration.x;
			particles[i].speed.y += particles[i].acceleration.y;
		}
	}
}

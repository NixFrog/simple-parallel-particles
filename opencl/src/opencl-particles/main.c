#include <stdio.h>
#include <CL/cl.h>
#include <stdlib.h>

#define MAX_SOURCE_SIZE (0x100000)

struct V2 {
	int x;
	int y;
};

struct Particle {
	V2 coordinates;
	V2 speed;
	V2 acceleration;
	int age;
};

struct FileData {
	int size;
	char* content;
};

struct ContextData {
	cl_context context;
	cl_device_id device_id;
};

FileData loadFile(char file_name[]) {
	FILE *fp;
	FileData file_data;

	/* Load the source code containing the kernel*/
	fp = fopen(file_name, "r");
	if (!fp) {
		fprintf(stderr, "Failed to load kernel.\n");
		exit(EXIT_FAILURE);
	}

	file_data.content = (char*)malloc(MAX_SOURCE_SIZE);
	file_data.size = fread(file_data.content, 1, MAX_SOURCE_SIZE, fp);

	fclose(fp);
	return file_data;
}

ContextData createContext() {
	ContextData context_data;
	cl_platform_id platform_id = NULL;
	cl_uint err_num_platforms, err_num_devices;

	cl_int err = clGetPlatformIDs(1, &platform_id, &err_num_platforms);
	err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &context_data.device_id, &err_num_devices);

	context_data.context = clCreateContext(NULL, 1, &context_data.device_id, NULL, NULL, &err);
	return context_data;
}

cl_program buildProgram(FileData file_data, ContextData context_data) {
	cl_program program = NULL;
	cl_int err;
	program = clCreateProgramWithSource(context_data.context, 1, (const char **)&file_data.content, (const size_t *)&file_data.size, &err);
	err = clBuildProgram(program, 1, &context_data.device_id, NULL, NULL, NULL);
	return program;
}

void init(Particle* particles, int beginIndex, int endIndex, int rangeX, int rangeY) {
	for (int i = beginIndex; i < endIndex; i++) {
		particles[i].coordinates.x = 0;
		particles[i].coordinates.y = 0;
		particles[i].speed.x = rand() % (rangeX * 2 + 1) - rangeX;
		particles[i].speed.y = rand() % rangeY;
		particles[i].acceleration.x = 0;
		particles[i].acceleration.y = -5;
		particles[i].age = 0;
	}
}

void release(cl_command_queue command_queue, cl_kernel kernel, cl_program program, cl_mem memobj) {
	clFlush(command_queue);
	clFinish(command_queue);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(memobj);
	clReleaseCommandQueue(command_queue);
}

void run(FileData file_data, ContextData context_data, int iterations, int generatedParticles) {
	cl_int err;
	cl_mem dParticles = NULL;
	cl_command_queue command_queue = NULL;
	int totalParticles = iterations * generatedParticles;
	int particlesSize = sizeof(Particle) * totalParticles;
	size_t localSize = 256;
	int lifespan = 100;
	size_t globalSize = ceil(totalParticles / (float)localSize) * localSize;

	Particle *hParticles = (Particle *)malloc(particlesSize);

	command_queue = clCreateCommandQueue(context_data.context, context_data.device_id, 0, &err);
	dParticles = clCreateBuffer(context_data.context, CL_MEM_READ_WRITE, particlesSize, NULL, &err);

	cl_program program = buildProgram(file_data, context_data);

	cl_kernel kernel = clCreateKernel(program, "update_particles", &err);
	clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&dParticles);

	init(hParticles, 0, totalParticles, 10, 10);

	/* Execute OpenCL Kernel */
	for (int i = 0; i < iterations; i++) {
		init(hParticles, generatedParticles * i, generatedParticles * (i + 1), 10, 10);
		int time = 1;
		err = clEnqueueWriteBuffer(command_queue, dParticles, CL_TRUE, 0, particlesSize, hParticles, 0, NULL, NULL);
		err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &dParticles);
		err |= clSetKernelArg(kernel, 1, sizeof(int), &totalParticles);
		err |= clSetKernelArg(kernel, 2, sizeof(int), &time);
		err |= clSetKernelArg(kernel, 3, sizeof(int), &lifespan);

		err = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

		clFinish(command_queue);
		clEnqueueReadBuffer(command_queue, dParticles, CL_TRUE, 0, particlesSize, hParticles, 0, NULL, NULL);
	}

	for (int i = 0; i<totalParticles; ++i) {
		printf("%d %d\n", hParticles[i].coordinates.x, hParticles[i].coordinates.y);
	}

	release(command_queue, kernel, program, dParticles);
	free(hParticles);
}

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Wrong number of arguments: <iterations> <particles generated>\n");
		exit(EXIT_FAILURE);
	}

	int iterations = atoi(argv[1]);
	int generatedParticles = atoi(argv[2]);
	FileData file_data = loadFile("./particles.cl");
	ContextData context_data = createContext();

	run(file_data, context_data, iterations, generatedParticles);
	clReleaseContext(context_data.context);

	free(file_data.content);

	return EXIT_SUCCESS;
}
